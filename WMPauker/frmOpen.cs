﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using VAkos;

namespace WMPauker
{
    public partial class frmOpen : Form
    {

        public frmOpen()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (lstFiles.SelectedIndex > 0)
                openFile(lstFiles.SelectedValue.ToString(), false);
        }

        private void frmOpen_Closed(object sender, EventArgs e)
        {
            frmMain.exitApplication();
        }

        private void menuItem1_Click(object sender, EventArgs e)
        {
            frmMain.exitApplication();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            openFile(txbFile.Text, true);
        }

        private void openFile(string fileName, Boolean doAddToHistory)
        {
            try
            {
                frmMain.paClass = new PaukerClass(fileName);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return;
            }

            if (doAddToHistory)
                addToHistory(fileName);

            frmMain.fileName = fileName;

            frmLessenOver f = new frmLessenOver();
            this.Hide();
            f.Show();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dlgOpen.ShowDialog() == DialogResult.OK)
                txbFile.Text = dlgOpen.FileName;
        }

        private void addToHistory(string fileName)
        {
            // here we add this file to history
            Xmlconfig config = new Xmlconfig("wmpauker.cfg", true);
            for (int i = 9; i > 0; i--)
            {

                if (config.Settings["history"]["File" + i.ToString()] != null)
                    config.Settings["history"]["File" + (i + 1).ToString()].Value =
                        config.Settings["history"]["File" + i.ToString()].Value;

            }

            config.Settings["history/File0"].Value = fileName;
        }

        private void loadHistory()
        {
            Xmlconfig config = new Xmlconfig("wmpauker.cfg", true);
            for (int i = 9; i > 0; i--)
            {

                if (config.Settings["history"]["File" + i.ToString()].Value != "")
                    lstFiles.Items.Add(config.Settings["history"]["File" + i.ToString()].Value);

            }
        }

        private void menuItem2_Click(object sender, EventArgs e)
        {
            frmMain.showAbout();
        }

        private void frmOpen_Load(object sender, EventArgs e)
        {
            loadHistory();
        }
    }
}