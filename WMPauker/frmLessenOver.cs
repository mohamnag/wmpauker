﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WMPauker
{
    public partial class frmLessenOver : Form
    {
        public frmLessenOver()
        {
            InitializeComponent();
        }

        private void progressBar2_ParentChanged(object sender, EventArgs e)
        {

        }

        private void frmLessenOver_Closed(object sender, EventArgs e)
        {
            frmMain.exitApplication();
        }

        private void menuItem4_Click(object sender, EventArgs e)
        {
            try
            {
                frmMain.exitApplication();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItem3_Click(object sender, EventArgs e)
        {
            frmMain.showAbout();
        }

        private void frmLessenOver_Load(object sender, EventArgs e)
        {
            assignStats();

        }

        private void assignStats()
        {
            pbarNotLearned.Maximum =
                pbarShortTermMem.Maximum =
                pbarUltraShortTermMem.Maximum =
                pbarBath1.Maximum =
                pbarBath2.Maximum =
                pbarBath3.Maximum =
                pbarBath4.Maximum =
                pbarBath5.Maximum =
                pbarBath6.Maximum =
                pbarBath7.Maximum =
                pbarBath8.Maximum =
                frmMain.paClass.Count;

            lblOverall.Text = pbarBath1.Maximum.ToString();
            lblDescription.Text = frmMain.paClass.Description;

            try
            {
                lblWholeBathNL.Text = Convert.ToString(pbarNotLearned.Value = frmMain.paClass.batchs["nl"].Count);
                mnuRptNl.Enabled = pbarNotLearned.Value > 0;

                lblExpiredBathUSM.Text = Convert.ToString(pbarUltraShortTermMem.Value2 = frmMain.paClass.batchs["usm"].expiredCount);
                lblWholeBathUSM.Text = Convert.ToString(pbarUltraShortTermMem.Value = frmMain.paClass.batchs["usm"].Count - pbarUltraShortTermMem.Value2);
                mnuRptUsm.Enabled = pbarUltraShortTermMem.Value2 > 0;

                lblExpiredBathSM.Text = Convert.ToString(pbarShortTermMem.Value2 = frmMain.paClass.batchs["stm"].expiredCount);
                lblWholeBathSM.Text = Convert.ToString(pbarShortTermMem.Value = frmMain.paClass.batchs["stm"].Count - pbarShortTermMem.Value2);
                mnuRptStm.Enabled = pbarShortTermMem.Value2 > 0;

                lblExpiredBath1.Text = Convert.ToString(pbarBath1.Value2 = frmMain.paClass.batchs["1"].expiredCount);
                lblWholeBath1.Text = Convert.ToString(pbarBath1.Value = frmMain.paClass.batchs["1"].Count - pbarBath1.Value2);
                mnuRptBath1.Enabled = pbarBath1.Value2 > 0;

                lblExpiredBath2.Text = Convert.ToString(pbarBath2.Value2 = frmMain.paClass.batchs["2"].expiredCount);
                lblWholeBath2.Text = Convert.ToString(pbarBath2.Value = frmMain.paClass.batchs["2"].Count - pbarBath2.Value2);
                mnuRptBath2.Enabled = pbarBath2.Value2 > 0;

                lblExpiredBath3.Text = Convert.ToString(pbarBath3.Value2 = frmMain.paClass.batchs["3"].expiredCount);
                lblWholeBath3.Text = Convert.ToString(pbarBath3.Value = frmMain.paClass.batchs["3"].Count - pbarBath3.Value2);
                mnuRptBath3.Enabled = pbarBath3.Value2 > 0;

                lblExpiredBath4.Text = Convert.ToString(pbarBath4.Value2 = frmMain.paClass.batchs["4"].expiredCount);
                lblWholeBath4.Text = Convert.ToString(pbarBath4.Value = frmMain.paClass.batchs["4"].Count - pbarBath4.Value2);
                mnuRptBath4.Enabled = pbarBath4.Value2 > 0;

                lblExpiredBath5.Text = Convert.ToString(pbarBath5.Value2 = frmMain.paClass.batchs["5"].expiredCount);
                lblWholeBath5.Text = Convert.ToString(pbarBath5.Value = frmMain.paClass.batchs["5"].Count - pbarBath5.Value2);
                mnuRptBath5.Enabled = pbarBath5.Value2 > 0;

                lblExpiredBath6.Text = Convert.ToString(pbarBath6.Value2 = frmMain.paClass.batchs["6"].expiredCount);
                lblWholeBath6.Text = Convert.ToString(pbarBath6.Value = frmMain.paClass.batchs["6"].Count - pbarBath6.Value2);
                mnuRptBath6.Enabled = pbarBath6.Value2 > 0;

                lblExpiredBath7.Text = Convert.ToString(pbarBath7.Value2 = frmMain.paClass.batchs["7"].expiredCount);
                lblWholeBath7.Text = Convert.ToString(pbarBath7.Value = frmMain.paClass.batchs["7"].Count - pbarBath7.Value2);
                mnuRptBath7.Enabled = pbarBath7.Value2 > 0;

                lblExpiredBath8.Text = Convert.ToString(pbarBath8.Value2 = frmMain.paClass.batchs["8"].expiredCount);
                lblWholeBath8.Text = Convert.ToString(pbarBath8.Value = frmMain.paClass.batchs["8"].Count - pbarBath8.Value2);
                //mnuRptBath8.Enabled = pbarBath8.Value2 > 0;
            }
            catch
            { }

            if (pbarUltraShortTermMem.Value2 > 0 || pbarShortTermMem.Value2 > 0)
            {
                MessageBox.Show("You have expired cards on batchs Ultra short term or Short term,\nit's highly recommended to repeat them now!",
                    "Attention", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
            }
        }

        private void mnuRptBath1_Click(object sender, EventArgs e)
        {
            repeatBatch("1");
        }

        private void repeatBatch(string batch)
        {
            repeatCard cardFrm = new repeatCard(batch);
            cardFrm.ShowDialog();

            assignStats();
        }

        private void mnuRptBath2_Click(object sender, EventArgs e)
        {
            repeatBatch("2");
        }

        private void mnuRptBath3_Click(object sender, EventArgs e)
        {
            repeatBatch("3");
        }

        private void mnuRptBath4_Click(object sender, EventArgs e)
        {
            repeatBatch("4");
        }

        private void mnuRptBath5_Click(object sender, EventArgs e)
        {
            repeatBatch("5");
        }

        private void mnuRptBath6_Click(object sender, EventArgs e)
        {
            repeatBatch("6");
        }

        private void mnuRptBath7_Click(object sender, EventArgs e)
        {
            repeatBatch("7");
        }

        private void mnuRptBath8_Click(object sender, EventArgs e)
        {
            repeatBatch("8");
        }



        private void mnuSave_Click(object sender, EventArgs e)
        {
            try
            {
                frmMain.paClass.saveToFile(frmMain.fileName);
                MessageBox.Show("File saved secessfuly.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnuSaveAs_Click(object sender, EventArgs e)
        {
            try
            {
                saveFileDialog.FileName = frmMain.fileName;
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    frmMain.paClass.saveToFile(saveFileDialog.FileName);
                    MessageBox.Show("File saved secessfuly.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnuRptNl_Click(object sender, EventArgs e)
        {
            repeatBatch("nl");
        }

        private void mnuRptUsm_Click(object sender, EventArgs e)
        {
            repeatBatch("usm");
        }

        private void mnuRptStm_Click(object sender, EventArgs e)
        {
            repeatBatch("stm");
        }

        private void frmLessenOver_Activated(object sender, EventArgs e)
        {
            tmrRefresh.Enabled = true;
        }

        private void frmLessenOver_Deactivate(object sender, EventArgs e)
        {
            tmrRefresh.Enabled = false;
        }

        private void tmrRefresh_Tick(object sender, EventArgs e)
        {
            assignStats();
        }

    }
}