﻿namespace WMPauker
{
    partial class repeatCard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mnuTest;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mnuTest = new System.Windows.Forms.MainMenu();
            this.mnuShowResult = new System.Windows.Forms.MenuItem();
            this.mnuStop = new System.Windows.Forms.MenuItem();
            this.txbFront = new System.Windows.Forms.TextBox();
            this.txbReverse = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblRemaining = new System.Windows.Forms.Label();
            this.lblTrue = new System.Windows.Forms.Label();
            this.lblFalse = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblTest = new System.Windows.Forms.Label();
            this.mnuResult = new System.Windows.Forms.MainMenu();
            this.mnuAprvCard = new System.Windows.Forms.MenuItem();
            this.mnuResetCard = new System.Windows.Forms.MenuItem();
            this.lblResult = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.mnuNl = new System.Windows.Forms.MainMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.lblNl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // mnuTest
            // 
            this.mnuTest.MenuItems.Add(this.mnuShowResult);
            this.mnuTest.MenuItems.Add(this.mnuStop);
            // 
            // mnuShowResult
            // 
            this.mnuShowResult.Text = "Show me!";
            this.mnuShowResult.Click += new System.EventHandler(this.mnuShowResult_Click);
            // 
            // mnuStop
            // 
            this.mnuStop.Text = "Stop";
            this.mnuStop.Click += new System.EventHandler(this.mnuStop_Click);
            // 
            // txbFront
            // 
            this.txbFront.Location = new System.Drawing.Point(7, 42);
            this.txbFront.Multiline = true;
            this.txbFront.Name = "txbFront";
            this.txbFront.ReadOnly = true;
            this.txbFront.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txbFront.Size = new System.Drawing.Size(227, 104);
            this.txbFront.TabIndex = 2;
            // 
            // txbReverse
            // 
            this.txbReverse.Location = new System.Drawing.Point(7, 150);
            this.txbReverse.Multiline = true;
            this.txbReverse.Name = "txbReverse";
            this.txbReverse.ReadOnly = true;
            this.txbReverse.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txbReverse.Size = new System.Drawing.Size(227, 104);
            this.txbReverse.TabIndex = 3;
            this.txbReverse.Visible = false;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.label1.ForeColor = System.Drawing.Color.YellowGreen;
            this.label1.Location = new System.Drawing.Point(5, 255);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 11);
            this.label1.Text = "remaining:";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.label2.ForeColor = System.Drawing.Color.ForestGreen;
            this.label2.Location = new System.Drawing.Point(89, 255);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 11);
            this.label2.Text = "learned:";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(155, 255);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 11);
            this.label3.Text = "not learned:";
            // 
            // lblRemaining
            // 
            this.lblRemaining.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.lblRemaining.ForeColor = System.Drawing.Color.YellowGreen;
            this.lblRemaining.Location = new System.Drawing.Point(53, 255);
            this.lblRemaining.Name = "lblRemaining";
            this.lblRemaining.Size = new System.Drawing.Size(30, 11);
            this.lblRemaining.Text = "10";
            // 
            // lblTrue
            // 
            this.lblTrue.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.lblTrue.ForeColor = System.Drawing.Color.ForestGreen;
            this.lblTrue.Location = new System.Drawing.Point(125, 255);
            this.lblTrue.Name = "lblTrue";
            this.lblTrue.Size = new System.Drawing.Size(30, 11);
            this.lblTrue.Text = "10";
            // 
            // lblFalse
            // 
            this.lblFalse.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.lblFalse.ForeColor = System.Drawing.Color.Red;
            this.lblFalse.Location = new System.Drawing.Point(207, 255);
            this.lblFalse.Name = "lblFalse";
            this.lblFalse.Size = new System.Drawing.Size(30, 11);
            this.lblFalse.Text = "10";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 50);
            // 
            // lblTest
            // 
            this.lblTest.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblTest.Location = new System.Drawing.Point(7, 23);
            this.lblTest.Name = "lblTest";
            this.lblTest.Size = new System.Drawing.Size(227, 20);
            this.lblTest.Text = "Do you remember the reverse side?";
            // 
            // mnuResult
            // 
            this.mnuResult.MenuItems.Add(this.mnuAprvCard);
            this.mnuResult.MenuItems.Add(this.mnuResetCard);
            // 
            // mnuAprvCard
            // 
            this.mnuAprvCard.Text = "Yes";
            this.mnuAprvCard.Click += new System.EventHandler(this.mnuAprvCard_Click);
            // 
            // mnuResetCard
            // 
            this.mnuResetCard.Text = "No";
            this.mnuResetCard.Click += new System.EventHandler(this.mnuResetCard_Click);
            // 
            // lblResult
            // 
            this.lblResult.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblResult.Location = new System.Drawing.Point(7, 23);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(227, 20);
            this.lblResult.Text = "Was your suggestion true?";
            // 
            // lblTitle
            // 
            this.lblTitle.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Italic);
            this.lblTitle.Location = new System.Drawing.Point(7, 3);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(227, 20);
            this.lblTitle.Text = "Repeating batch: ";
            // 
            // mnuNl
            // 
            this.mnuNl.MenuItems.Add(this.menuItem1);
            this.mnuNl.MenuItems.Add(this.menuItem2);
            // 
            // menuItem1
            // 
            this.menuItem1.Text = "Next";
            this.menuItem1.Click += new System.EventHandler(this.mnuAprvCard_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Text = "Stop";
            this.menuItem2.Click += new System.EventHandler(this.mnuStop_Click);
            // 
            // lblNl
            // 
            this.lblNl.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblNl.Location = new System.Drawing.Point(5, 23);
            this.lblNl.Name = "lblNl";
            this.lblNl.Size = new System.Drawing.Size(227, 20);
            this.lblNl.Text = "Memmorise the card:";
            // 
            // repeatCard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.lblFalse);
            this.Controls.Add(this.lblTrue);
            this.Controls.Add(this.lblRemaining);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txbFront);
            this.Controls.Add(this.txbReverse);
            this.Controls.Add(this.lblTest);
            this.Controls.Add(this.lblResult);
            this.Controls.Add(this.lblNl);
            this.KeyPreview = true;
            this.Menu = this.mnuResult;
            this.Name = "repeatCard";
            this.Text = "Repeat Card";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txbFront;
        private System.Windows.Forms.TextBox txbReverse;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblRemaining;
        private System.Windows.Forms.Label lblTrue;
        private System.Windows.Forms.Label lblFalse;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblTest;
        private System.Windows.Forms.MenuItem mnuShowResult;
        private System.Windows.Forms.MenuItem mnuStop;
        private System.Windows.Forms.MainMenu mnuResult;
        private System.Windows.Forms.MenuItem mnuAprvCard;
        private System.Windows.Forms.MenuItem mnuResetCard;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.MainMenu mnuNl;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem menuItem2;
        private System.Windows.Forms.Label lblNl;
    }
}