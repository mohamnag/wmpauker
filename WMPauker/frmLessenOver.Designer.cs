﻿namespace WMPauker
{
    partial class frmLessenOver
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLessenOver));
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem6 = new System.Windows.Forms.MenuItem();
            this.mnuRptBath1 = new System.Windows.Forms.MenuItem();
            this.mnuRptBath2 = new System.Windows.Forms.MenuItem();
            this.mnuRptBath3 = new System.Windows.Forms.MenuItem();
            this.mnuRptBath4 = new System.Windows.Forms.MenuItem();
            this.mnuRptBath5 = new System.Windows.Forms.MenuItem();
            this.mnuRptBath6 = new System.Windows.Forms.MenuItem();
            this.mnuRptBath7 = new System.Windows.Forms.MenuItem();
            this.mnuRptLearnNew = new System.Windows.Forms.MenuItem();
            this.mnuRptNl = new System.Windows.Forms.MenuItem();
            this.mnuRptUsm = new System.Windows.Forms.MenuItem();
            this.mnuRptStm = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.menuItem8 = new System.Windows.Forms.MenuItem();
            this.menuItem7 = new System.Windows.Forms.MenuItem();
            this.mnuSave = new System.Windows.Forms.MenuItem();
            this.mnuSaveAs = new System.Windows.Forms.MenuItem();
            this.menuItem9 = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblExpiredBath8 = new System.Windows.Forms.Label();
            this.lblExpiredBath7 = new System.Windows.Forms.Label();
            this.lblExpiredBath6 = new System.Windows.Forms.Label();
            this.lblExpiredBath5 = new System.Windows.Forms.Label();
            this.lblExpiredBath4 = new System.Windows.Forms.Label();
            this.lblExpiredBath3 = new System.Windows.Forms.Label();
            this.lblExpiredBath2 = new System.Windows.Forms.Label();
            this.lblExpiredBath1 = new System.Windows.Forms.Label();
            this.lblWholeBath8 = new System.Windows.Forms.Label();
            this.lblWholeBath7 = new System.Windows.Forms.Label();
            this.lblWholeBath6 = new System.Windows.Forms.Label();
            this.lblWholeBath5 = new System.Windows.Forms.Label();
            this.lblWholeBath4 = new System.Windows.Forms.Label();
            this.lblWholeBath3 = new System.Windows.Forms.Label();
            this.lblWholeBath2 = new System.Windows.Forms.Label();
            this.lblWholeBath1 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.lblWholeBathSM = new System.Windows.Forms.Label();
            this.lblWholeBathUSM = new System.Windows.Forms.Label();
            this.lblWholeBathNL = new System.Windows.Forms.Label();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.lblExpiredBathSM = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblExpiredBathUSM = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.pbarUltraShortTermMem = new Bornander.UI.ProgressBar.ProgressBar();
            this.pbarShortTermMem = new Bornander.UI.ProgressBar.ProgressBar();
            this.pbarNotLearned = new Bornander.UI.ProgressBar.ProgressBar();
            this.pbarBath1 = new Bornander.UI.ProgressBar.ProgressBar();
            this.pbarBath2 = new Bornander.UI.ProgressBar.ProgressBar();
            this.pbarBath3 = new Bornander.UI.ProgressBar.ProgressBar();
            this.pbarBath4 = new Bornander.UI.ProgressBar.ProgressBar();
            this.pbarBath5 = new Bornander.UI.ProgressBar.ProgressBar();
            this.pbarBath6 = new Bornander.UI.ProgressBar.ProgressBar();
            this.pbarBath7 = new Bornander.UI.ProgressBar.ProgressBar();
            this.pbarBath8 = new Bornander.UI.ProgressBar.ProgressBar();
            this.tmrRefresh = new System.Windows.Forms.Timer();
            this.lblOverall = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            this.mainMenu1.MenuItems.Add(this.menuItem2);
            // 
            // menuItem1
            // 
            this.menuItem1.MenuItems.Add(this.menuItem6);
            this.menuItem1.MenuItems.Add(this.mnuRptLearnNew);
            this.menuItem1.Text = "Learn";
            // 
            // menuItem6
            // 
            this.menuItem6.MenuItems.Add(this.mnuRptBath1);
            this.menuItem6.MenuItems.Add(this.mnuRptBath2);
            this.menuItem6.MenuItems.Add(this.mnuRptBath3);
            this.menuItem6.MenuItems.Add(this.mnuRptBath4);
            this.menuItem6.MenuItems.Add(this.mnuRptBath5);
            this.menuItem6.MenuItems.Add(this.mnuRptBath6);
            this.menuItem6.MenuItems.Add(this.mnuRptBath7);
            this.menuItem6.Text = "Repeat expired cards";
            // 
            // mnuRptBath1
            // 
            this.mnuRptBath1.Enabled = false;
            this.mnuRptBath1.Text = "Bath 1";
            this.mnuRptBath1.Click += new System.EventHandler(this.mnuRptBath1_Click);
            // 
            // mnuRptBath2
            // 
            this.mnuRptBath2.Enabled = false;
            this.mnuRptBath2.Text = "Bath 2";
            this.mnuRptBath2.Click += new System.EventHandler(this.mnuRptBath2_Click);
            // 
            // mnuRptBath3
            // 
            this.mnuRptBath3.Enabled = false;
            this.mnuRptBath3.Text = "Bath 3";
            this.mnuRptBath3.Click += new System.EventHandler(this.mnuRptBath3_Click);
            // 
            // mnuRptBath4
            // 
            this.mnuRptBath4.Enabled = false;
            this.mnuRptBath4.Text = "Bath 4";
            this.mnuRptBath4.Click += new System.EventHandler(this.mnuRptBath4_Click);
            // 
            // mnuRptBath5
            // 
            this.mnuRptBath5.Enabled = false;
            this.mnuRptBath5.Text = "Bath 5";
            this.mnuRptBath5.Click += new System.EventHandler(this.mnuRptBath5_Click);
            // 
            // mnuRptBath6
            // 
            this.mnuRptBath6.Enabled = false;
            this.mnuRptBath6.Text = "Bath 6";
            this.mnuRptBath6.Click += new System.EventHandler(this.mnuRptBath6_Click);
            // 
            // mnuRptBath7
            // 
            this.mnuRptBath7.Enabled = false;
            this.mnuRptBath7.Text = "Bath 7";
            this.mnuRptBath7.Click += new System.EventHandler(this.mnuRptBath7_Click);
            // 
            // mnuRptLearnNew
            // 
            this.mnuRptLearnNew.MenuItems.Add(this.mnuRptNl);
            this.mnuRptLearnNew.MenuItems.Add(this.mnuRptUsm);
            this.mnuRptLearnNew.MenuItems.Add(this.mnuRptStm);
            this.mnuRptLearnNew.Text = "Repeat new cards";
            // 
            // mnuRptNl
            // 
            this.mnuRptNl.Enabled = false;
            this.mnuRptNl.Text = "Not learned";
            this.mnuRptNl.Click += new System.EventHandler(this.mnuRptNl_Click);
            // 
            // mnuRptUsm
            // 
            this.mnuRptUsm.Enabled = false;
            this.mnuRptUsm.Text = "Ultra short term";
            this.mnuRptUsm.Click += new System.EventHandler(this.mnuRptUsm_Click);
            // 
            // mnuRptStm
            // 
            this.mnuRptStm.Enabled = false;
            this.mnuRptStm.Text = "Short term";
            this.mnuRptStm.Click += new System.EventHandler(this.mnuRptStm_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.MenuItems.Add(this.menuItem8);
            this.menuItem2.MenuItems.Add(this.menuItem7);
            this.menuItem2.MenuItems.Add(this.mnuSave);
            this.menuItem2.MenuItems.Add(this.mnuSaveAs);
            this.menuItem2.MenuItems.Add(this.menuItem9);
            this.menuItem2.MenuItems.Add(this.menuItem3);
            this.menuItem2.MenuItems.Add(this.menuItem4);
            this.menuItem2.Text = "Menu";
            // 
            // menuItem8
            // 
            this.menuItem8.Enabled = false;
            this.menuItem8.Text = "Add new card";
            // 
            // menuItem7
            // 
            this.menuItem7.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveAs
            // 
            this.mnuSaveAs.Text = "Save as . . .";
            this.mnuSaveAs.Click += new System.EventHandler(this.mnuSaveAs_Click);
            // 
            // menuItem9
            // 
            this.menuItem9.Text = "-";
            // 
            // menuItem3
            // 
            this.menuItem3.Text = "About";
            this.menuItem3.Click += new System.EventHandler(this.menuItem3_Click);
            // 
            // menuItem4
            // 
            this.menuItem4.Text = "Exit";
            this.menuItem4.Click += new System.EventHandler(this.menuItem4_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(17, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(12, 16);
            this.label1.Text = "1";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(17, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(12, 16);
            this.label2.Text = "2";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(17, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(12, 16);
            this.label3.Text = "3";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(17, 111);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(12, 16);
            this.label4.Text = "4";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(17, 175);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(12, 16);
            this.label5.Text = "8";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(17, 159);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(12, 16);
            this.label6.Text = "7";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(17, 143);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(12, 16);
            this.label7.Text = "6";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(17, 127);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(12, 16);
            this.label8.Text = "5";
            // 
            // label9
            // 
            this.label9.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label9.Location = new System.Drawing.Point(2, 39);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(27, 16);
            this.label9.Text = "SM";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label10
            // 
            this.label10.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label10.Location = new System.Drawing.Point(-10, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 16);
            this.label10.Text = "USM";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label11
            // 
            this.label11.ForeColor = System.Drawing.Color.MediumOrchid;
            this.label11.Location = new System.Drawing.Point(2, 7);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(27, 16);
            this.label11.Text = "NL";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblDescription
            // 
            this.lblDescription.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Italic);
            this.lblDescription.Location = new System.Drawing.Point(8, 214);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(228, 54);
            this.lblDescription.Text = "Some thing about this lessen. Some thing about this lessen. Some thing about this" +
                " lessen. Some thing about this lessen. ";
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(3, 198);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(136, 16);
            this.label13.Text = "Lessen Description:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.Location = new System.Drawing.Point(8, 59);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(224, 1);
            // 
            // lblExpiredBath8
            // 
            this.lblExpiredBath8.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblExpiredBath8.ForeColor = System.Drawing.Color.Red;
            this.lblExpiredBath8.Location = new System.Drawing.Point(211, 175);
            this.lblExpiredBath8.Name = "lblExpiredBath8";
            this.lblExpiredBath8.Size = new System.Drawing.Size(25, 16);
            this.lblExpiredBath8.Text = " ";
            // 
            // lblExpiredBath7
            // 
            this.lblExpiredBath7.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblExpiredBath7.ForeColor = System.Drawing.Color.Red;
            this.lblExpiredBath7.Location = new System.Drawing.Point(211, 159);
            this.lblExpiredBath7.Name = "lblExpiredBath7";
            this.lblExpiredBath7.Size = new System.Drawing.Size(25, 16);
            this.lblExpiredBath7.Text = " ";
            // 
            // lblExpiredBath6
            // 
            this.lblExpiredBath6.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblExpiredBath6.ForeColor = System.Drawing.Color.Red;
            this.lblExpiredBath6.Location = new System.Drawing.Point(211, 143);
            this.lblExpiredBath6.Name = "lblExpiredBath6";
            this.lblExpiredBath6.Size = new System.Drawing.Size(25, 16);
            this.lblExpiredBath6.Text = " ";
            // 
            // lblExpiredBath5
            // 
            this.lblExpiredBath5.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblExpiredBath5.ForeColor = System.Drawing.Color.Red;
            this.lblExpiredBath5.Location = new System.Drawing.Point(211, 127);
            this.lblExpiredBath5.Name = "lblExpiredBath5";
            this.lblExpiredBath5.Size = new System.Drawing.Size(25, 16);
            this.lblExpiredBath5.Text = " ";
            // 
            // lblExpiredBath4
            // 
            this.lblExpiredBath4.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblExpiredBath4.ForeColor = System.Drawing.Color.Red;
            this.lblExpiredBath4.Location = new System.Drawing.Point(211, 111);
            this.lblExpiredBath4.Name = "lblExpiredBath4";
            this.lblExpiredBath4.Size = new System.Drawing.Size(25, 16);
            this.lblExpiredBath4.Text = " ";
            // 
            // lblExpiredBath3
            // 
            this.lblExpiredBath3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblExpiredBath3.ForeColor = System.Drawing.Color.Red;
            this.lblExpiredBath3.Location = new System.Drawing.Point(211, 95);
            this.lblExpiredBath3.Name = "lblExpiredBath3";
            this.lblExpiredBath3.Size = new System.Drawing.Size(25, 16);
            this.lblExpiredBath3.Text = " ";
            // 
            // lblExpiredBath2
            // 
            this.lblExpiredBath2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblExpiredBath2.ForeColor = System.Drawing.Color.Red;
            this.lblExpiredBath2.Location = new System.Drawing.Point(211, 79);
            this.lblExpiredBath2.Name = "lblExpiredBath2";
            this.lblExpiredBath2.Size = new System.Drawing.Size(25, 16);
            this.lblExpiredBath2.Text = " ";
            // 
            // lblExpiredBath1
            // 
            this.lblExpiredBath1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblExpiredBath1.ForeColor = System.Drawing.Color.Red;
            this.lblExpiredBath1.Location = new System.Drawing.Point(211, 63);
            this.lblExpiredBath1.Name = "lblExpiredBath1";
            this.lblExpiredBath1.Size = new System.Drawing.Size(25, 16);
            this.lblExpiredBath1.Text = " ";
            // 
            // lblWholeBath8
            // 
            this.lblWholeBath8.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblWholeBath8.ForeColor = System.Drawing.Color.Gray;
            this.lblWholeBath8.Location = new System.Drawing.Point(186, 175);
            this.lblWholeBath8.Name = "lblWholeBath8";
            this.lblWholeBath8.Size = new System.Drawing.Size(21, 16);
            this.lblWholeBath8.Text = " ";
            this.lblWholeBath8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblWholeBath7
            // 
            this.lblWholeBath7.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblWholeBath7.ForeColor = System.Drawing.Color.Gray;
            this.lblWholeBath7.Location = new System.Drawing.Point(186, 159);
            this.lblWholeBath7.Name = "lblWholeBath7";
            this.lblWholeBath7.Size = new System.Drawing.Size(21, 16);
            this.lblWholeBath7.Text = " ";
            this.lblWholeBath7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblWholeBath6
            // 
            this.lblWholeBath6.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblWholeBath6.ForeColor = System.Drawing.Color.Gray;
            this.lblWholeBath6.Location = new System.Drawing.Point(186, 143);
            this.lblWholeBath6.Name = "lblWholeBath6";
            this.lblWholeBath6.Size = new System.Drawing.Size(21, 16);
            this.lblWholeBath6.Text = " ";
            this.lblWholeBath6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblWholeBath5
            // 
            this.lblWholeBath5.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblWholeBath5.ForeColor = System.Drawing.Color.Gray;
            this.lblWholeBath5.Location = new System.Drawing.Point(186, 127);
            this.lblWholeBath5.Name = "lblWholeBath5";
            this.lblWholeBath5.Size = new System.Drawing.Size(21, 16);
            this.lblWholeBath5.Text = " ";
            this.lblWholeBath5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblWholeBath4
            // 
            this.lblWholeBath4.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblWholeBath4.ForeColor = System.Drawing.Color.Gray;
            this.lblWholeBath4.Location = new System.Drawing.Point(186, 111);
            this.lblWholeBath4.Name = "lblWholeBath4";
            this.lblWholeBath4.Size = new System.Drawing.Size(21, 16);
            this.lblWholeBath4.Text = " ";
            this.lblWholeBath4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblWholeBath3
            // 
            this.lblWholeBath3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblWholeBath3.ForeColor = System.Drawing.Color.Gray;
            this.lblWholeBath3.Location = new System.Drawing.Point(186, 95);
            this.lblWholeBath3.Name = "lblWholeBath3";
            this.lblWholeBath3.Size = new System.Drawing.Size(21, 16);
            this.lblWholeBath3.Text = " ";
            this.lblWholeBath3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblWholeBath2
            // 
            this.lblWholeBath2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblWholeBath2.ForeColor = System.Drawing.Color.Gray;
            this.lblWholeBath2.Location = new System.Drawing.Point(186, 79);
            this.lblWholeBath2.Name = "lblWholeBath2";
            this.lblWholeBath2.Size = new System.Drawing.Size(21, 16);
            this.lblWholeBath2.Text = " ";
            this.lblWholeBath2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblWholeBath1
            // 
            this.lblWholeBath1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblWholeBath1.ForeColor = System.Drawing.Color.Gray;
            this.lblWholeBath1.Location = new System.Drawing.Point(186, 63);
            this.lblWholeBath1.Name = "lblWholeBath1";
            this.lblWholeBath1.Size = new System.Drawing.Size(21, 16);
            this.lblWholeBath1.Text = " ";
            this.lblWholeBath1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label21.Location = new System.Drawing.Point(205, 175);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(6, 16);
            this.label21.Text = "/";
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label22.Location = new System.Drawing.Point(205, 159);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(6, 16);
            this.label22.Text = "/";
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label23.Location = new System.Drawing.Point(205, 143);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(6, 16);
            this.label23.Text = "/";
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label24.Location = new System.Drawing.Point(205, 127);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(6, 16);
            this.label24.Text = "/";
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label25.Location = new System.Drawing.Point(205, 111);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(6, 16);
            this.label25.Text = "/";
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label26.Location = new System.Drawing.Point(205, 95);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(6, 16);
            this.label26.Text = "/";
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label27.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label27.Location = new System.Drawing.Point(205, 79);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(6, 16);
            this.label27.Text = "/";
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label28.Location = new System.Drawing.Point(205, 63);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(6, 16);
            this.label28.Text = "/";
            // 
            // lblWholeBathSM
            // 
            this.lblWholeBathSM.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblWholeBathSM.ForeColor = System.Drawing.Color.Gray;
            this.lblWholeBathSM.Location = new System.Drawing.Point(186, 39);
            this.lblWholeBathSM.Name = "lblWholeBathSM";
            this.lblWholeBathSM.Size = new System.Drawing.Size(21, 16);
            this.lblWholeBathSM.Text = " ";
            this.lblWholeBathSM.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblWholeBathUSM
            // 
            this.lblWholeBathUSM.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblWholeBathUSM.ForeColor = System.Drawing.Color.Gray;
            this.lblWholeBathUSM.Location = new System.Drawing.Point(186, 23);
            this.lblWholeBathUSM.Name = "lblWholeBathUSM";
            this.lblWholeBathUSM.Size = new System.Drawing.Size(21, 16);
            this.lblWholeBathUSM.Text = " ";
            this.lblWholeBathUSM.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblWholeBathNL
            // 
            this.lblWholeBathNL.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblWholeBathNL.ForeColor = System.Drawing.Color.Gray;
            this.lblWholeBathNL.Location = new System.Drawing.Point(186, 7);
            this.lblWholeBathNL.Name = "lblWholeBathNL";
            this.lblWholeBathNL.Size = new System.Drawing.Size(21, 16);
            this.lblWholeBathNL.Text = " ";
            this.lblWholeBathNL.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.Filter = "Pauker File|*.pau|Pauker Compressed File|*.pau.gz";
            this.saveFileDialog.FilterIndex = 2;
            // 
            // lblExpiredBathSM
            // 
            this.lblExpiredBathSM.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblExpiredBathSM.ForeColor = System.Drawing.Color.Red;
            this.lblExpiredBathSM.Location = new System.Drawing.Point(211, 39);
            this.lblExpiredBathSM.Name = "lblExpiredBathSM";
            this.lblExpiredBathSM.Size = new System.Drawing.Size(25, 16);
            this.lblExpiredBathSM.Text = " ";
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label14.Location = new System.Drawing.Point(205, 39);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(6, 16);
            this.label14.Text = "/";
            // 
            // lblExpiredBathUSM
            // 
            this.lblExpiredBathUSM.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblExpiredBathUSM.ForeColor = System.Drawing.Color.Red;
            this.lblExpiredBathUSM.Location = new System.Drawing.Point(211, 23);
            this.lblExpiredBathUSM.Name = "lblExpiredBathUSM";
            this.lblExpiredBathUSM.Size = new System.Drawing.Size(25, 16);
            this.lblExpiredBathUSM.Text = " ";
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label16.Location = new System.Drawing.Point(205, 23);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(6, 16);
            this.label16.Text = "/";
            // 
            // pbarUltraShortTermMem
            // 
            this.pbarUltraShortTermMem.BackgroundDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarUltraShortTermMem.BackgroundLeadingSize = 2;
            this.pbarUltraShortTermMem.BackgroundPicture = ((System.Drawing.Image)(resources.GetObject("pbarUltraShortTermMem.BackgroundPicture")));
            this.pbarUltraShortTermMem.BackgroundTrailingSize = 2;
            this.pbarUltraShortTermMem.ForegroundDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarUltraShortTermMem.ForegroundDrawMethod2 = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarUltraShortTermMem.ForegroundLeadingSize = 0;
            this.pbarUltraShortTermMem.ForegroundLeadingSize2 = 0;
            this.pbarUltraShortTermMem.ForegroundPicture = ((System.Drawing.Image)(resources.GetObject("pbarUltraShortTermMem.ForegroundPicture")));
            this.pbarUltraShortTermMem.ForegroundPicture2 = ((System.Drawing.Image)(resources.GetObject("pbarUltraShortTermMem.ForegroundPicture2")));
            this.pbarUltraShortTermMem.ForegroundTrailingSize = 0;
            this.pbarUltraShortTermMem.ForegroundTrailingSize2 = 0;
            this.pbarUltraShortTermMem.Location = new System.Drawing.Point(35, 26);
            this.pbarUltraShortTermMem.Marquee = Bornander.UI.ProgressBar.ProgressBar.MarqueeStyle.Wave;
            this.pbarUltraShortTermMem.MarqueeWidth = 0;
            this.pbarUltraShortTermMem.Maximum = 660;
            this.pbarUltraShortTermMem.Minimum = 0;
            this.pbarUltraShortTermMem.Name = "pbarUltraShortTermMem";
            this.pbarUltraShortTermMem.OverlayDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarUltraShortTermMem.OverlayLeadingSize = 0;
            this.pbarUltraShortTermMem.OverlayPicture = null;
            this.pbarUltraShortTermMem.OverlayTrailingSize = 0;
            this.pbarUltraShortTermMem.Size = new System.Drawing.Size(148, 10);
            this.pbarUltraShortTermMem.Type = Bornander.UI.ProgressBar.ProgressBar.BarType.Progress;
            this.pbarUltraShortTermMem.Value = 0;
            this.pbarUltraShortTermMem.Value2 = 0;
            // 
            // pbarShortTermMem
            // 
            this.pbarShortTermMem.BackgroundDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarShortTermMem.BackgroundLeadingSize = 2;
            this.pbarShortTermMem.BackgroundPicture = ((System.Drawing.Image)(resources.GetObject("pbarShortTermMem.BackgroundPicture")));
            this.pbarShortTermMem.BackgroundTrailingSize = 2;
            this.pbarShortTermMem.ForegroundDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarShortTermMem.ForegroundDrawMethod2 = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarShortTermMem.ForegroundLeadingSize = 0;
            this.pbarShortTermMem.ForegroundLeadingSize2 = 0;
            this.pbarShortTermMem.ForegroundPicture = ((System.Drawing.Image)(resources.GetObject("pbarShortTermMem.ForegroundPicture")));
            this.pbarShortTermMem.ForegroundPicture2 = ((System.Drawing.Image)(resources.GetObject("pbarShortTermMem.ForegroundPicture2")));
            this.pbarShortTermMem.ForegroundTrailingSize = 0;
            this.pbarShortTermMem.ForegroundTrailingSize2 = 0;
            this.pbarShortTermMem.Location = new System.Drawing.Point(35, 42);
            this.pbarShortTermMem.Marquee = Bornander.UI.ProgressBar.ProgressBar.MarqueeStyle.Wave;
            this.pbarShortTermMem.MarqueeWidth = 0;
            this.pbarShortTermMem.Maximum = 660;
            this.pbarShortTermMem.Minimum = 0;
            this.pbarShortTermMem.Name = "pbarShortTermMem";
            this.pbarShortTermMem.OverlayDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarShortTermMem.OverlayLeadingSize = 0;
            this.pbarShortTermMem.OverlayPicture = null;
            this.pbarShortTermMem.OverlayTrailingSize = 0;
            this.pbarShortTermMem.Size = new System.Drawing.Size(148, 10);
            this.pbarShortTermMem.Type = Bornander.UI.ProgressBar.ProgressBar.BarType.Progress;
            this.pbarShortTermMem.Value = 0;
            this.pbarShortTermMem.Value2 = 0;
            // 
            // pbarNotLearned
            // 
            this.pbarNotLearned.BackgroundDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarNotLearned.BackgroundLeadingSize = 2;
            this.pbarNotLearned.BackgroundPicture = ((System.Drawing.Image)(resources.GetObject("pbarNotLearned.BackgroundPicture")));
            this.pbarNotLearned.BackgroundTrailingSize = 2;
            this.pbarNotLearned.ForegroundDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarNotLearned.ForegroundDrawMethod2 = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Tile;
            this.pbarNotLearned.ForegroundLeadingSize = 2;
            this.pbarNotLearned.ForegroundLeadingSize2 = 0;
            this.pbarNotLearned.ForegroundPicture = ((System.Drawing.Image)(resources.GetObject("pbarNotLearned.ForegroundPicture")));
            this.pbarNotLearned.ForegroundPicture2 = ((System.Drawing.Image)(resources.GetObject("pbarNotLearned.ForegroundPicture2")));
            this.pbarNotLearned.ForegroundTrailingSize = 2;
            this.pbarNotLearned.ForegroundTrailingSize2 = 0;
            this.pbarNotLearned.Location = new System.Drawing.Point(35, 10);
            this.pbarNotLearned.Marquee = Bornander.UI.ProgressBar.ProgressBar.MarqueeStyle.TileWrap;
            this.pbarNotLearned.MarqueeWidth = 10;
            this.pbarNotLearned.Maximum = 100;
            this.pbarNotLearned.Minimum = 0;
            this.pbarNotLearned.Name = "pbarNotLearned";
            this.pbarNotLearned.OverlayDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarNotLearned.OverlayLeadingSize = 4;
            this.pbarNotLearned.OverlayPicture = null;
            this.pbarNotLearned.OverlayTrailingSize = 4;
            this.pbarNotLearned.Size = new System.Drawing.Size(148, 10);
            this.pbarNotLearned.Type = Bornander.UI.ProgressBar.ProgressBar.BarType.Progress;
            this.pbarNotLearned.Value = 0;
            this.pbarNotLearned.Value2 = 0;
            // 
            // pbarBath1
            // 
            this.pbarBath1.BackgroundDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath1.BackgroundLeadingSize = 2;
            this.pbarBath1.BackgroundPicture = ((System.Drawing.Image)(resources.GetObject("pbarBath1.BackgroundPicture")));
            this.pbarBath1.BackgroundTrailingSize = 2;
            this.pbarBath1.ForegroundDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath1.ForegroundDrawMethod2 = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath1.ForegroundLeadingSize = 0;
            this.pbarBath1.ForegroundLeadingSize2 = 0;
            this.pbarBath1.ForegroundPicture = ((System.Drawing.Image)(resources.GetObject("pbarBath1.ForegroundPicture")));
            this.pbarBath1.ForegroundPicture2 = ((System.Drawing.Image)(resources.GetObject("pbarBath1.ForegroundPicture2")));
            this.pbarBath1.ForegroundTrailingSize = 0;
            this.pbarBath1.ForegroundTrailingSize2 = 0;
            this.pbarBath1.Location = new System.Drawing.Point(35, 66);
            this.pbarBath1.Marquee = Bornander.UI.ProgressBar.ProgressBar.MarqueeStyle.Wave;
            this.pbarBath1.MarqueeWidth = 0;
            this.pbarBath1.Maximum = 660;
            this.pbarBath1.Minimum = 0;
            this.pbarBath1.Name = "pbarBath1";
            this.pbarBath1.OverlayDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath1.OverlayLeadingSize = 0;
            this.pbarBath1.OverlayPicture = null;
            this.pbarBath1.OverlayTrailingSize = 0;
            this.pbarBath1.Size = new System.Drawing.Size(148, 10);
            this.pbarBath1.Type = Bornander.UI.ProgressBar.ProgressBar.BarType.Progress;
            this.pbarBath1.Value = 0;
            this.pbarBath1.Value2 = 0;
            // 
            // pbarBath2
            // 
            this.pbarBath2.BackgroundDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath2.BackgroundLeadingSize = 2;
            this.pbarBath2.BackgroundPicture = ((System.Drawing.Image)(resources.GetObject("pbarBath2.BackgroundPicture")));
            this.pbarBath2.BackgroundTrailingSize = 2;
            this.pbarBath2.ForegroundDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath2.ForegroundDrawMethod2 = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath2.ForegroundLeadingSize = 2;
            this.pbarBath2.ForegroundLeadingSize2 = 2;
            this.pbarBath2.ForegroundPicture = ((System.Drawing.Image)(resources.GetObject("pbarBath2.ForegroundPicture")));
            this.pbarBath2.ForegroundPicture2 = ((System.Drawing.Image)(resources.GetObject("pbarBath2.ForegroundPicture2")));
            this.pbarBath2.ForegroundTrailingSize = 2;
            this.pbarBath2.ForegroundTrailingSize2 = 2;
            this.pbarBath2.Location = new System.Drawing.Point(35, 82);
            this.pbarBath2.Marquee = Bornander.UI.ProgressBar.ProgressBar.MarqueeStyle.TileWrap;
            this.pbarBath2.MarqueeWidth = 10;
            this.pbarBath2.Maximum = 100;
            this.pbarBath2.Minimum = 0;
            this.pbarBath2.Name = "pbarBath2";
            this.pbarBath2.OverlayDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath2.OverlayLeadingSize = 4;
            this.pbarBath2.OverlayPicture = null;
            this.pbarBath2.OverlayTrailingSize = 4;
            this.pbarBath2.Size = new System.Drawing.Size(148, 10);
            this.pbarBath2.Type = Bornander.UI.ProgressBar.ProgressBar.BarType.Progress;
            this.pbarBath2.Value = 0;
            this.pbarBath2.Value2 = 0;
            // 
            // pbarBath3
            // 
            this.pbarBath3.BackgroundDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath3.BackgroundLeadingSize = 2;
            this.pbarBath3.BackgroundPicture = ((System.Drawing.Image)(resources.GetObject("pbarBath3.BackgroundPicture")));
            this.pbarBath3.BackgroundTrailingSize = 2;
            this.pbarBath3.ForegroundDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath3.ForegroundDrawMethod2 = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath3.ForegroundLeadingSize = 2;
            this.pbarBath3.ForegroundLeadingSize2 = 2;
            this.pbarBath3.ForegroundPicture = ((System.Drawing.Image)(resources.GetObject("pbarBath3.ForegroundPicture")));
            this.pbarBath3.ForegroundPicture2 = ((System.Drawing.Image)(resources.GetObject("pbarBath3.ForegroundPicture2")));
            this.pbarBath3.ForegroundTrailingSize = 2;
            this.pbarBath3.ForegroundTrailingSize2 = 2;
            this.pbarBath3.Location = new System.Drawing.Point(35, 98);
            this.pbarBath3.Marquee = Bornander.UI.ProgressBar.ProgressBar.MarqueeStyle.TileWrap;
            this.pbarBath3.MarqueeWidth = 10;
            this.pbarBath3.Maximum = 100;
            this.pbarBath3.Minimum = 0;
            this.pbarBath3.Name = "pbarBath3";
            this.pbarBath3.OverlayDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath3.OverlayLeadingSize = 4;
            this.pbarBath3.OverlayPicture = null;
            this.pbarBath3.OverlayTrailingSize = 4;
            this.pbarBath3.Size = new System.Drawing.Size(148, 10);
            this.pbarBath3.Type = Bornander.UI.ProgressBar.ProgressBar.BarType.Progress;
            this.pbarBath3.Value = 0;
            this.pbarBath3.Value2 = 0;
            // 
            // pbarBath4
            // 
            this.pbarBath4.BackgroundDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath4.BackgroundLeadingSize = 2;
            this.pbarBath4.BackgroundPicture = ((System.Drawing.Image)(resources.GetObject("pbarBath4.BackgroundPicture")));
            this.pbarBath4.BackgroundTrailingSize = 2;
            this.pbarBath4.ForegroundDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath4.ForegroundDrawMethod2 = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath4.ForegroundLeadingSize = 2;
            this.pbarBath4.ForegroundLeadingSize2 = 2;
            this.pbarBath4.ForegroundPicture = ((System.Drawing.Image)(resources.GetObject("pbarBath4.ForegroundPicture")));
            this.pbarBath4.ForegroundPicture2 = ((System.Drawing.Image)(resources.GetObject("pbarBath4.ForegroundPicture2")));
            this.pbarBath4.ForegroundTrailingSize = 2;
            this.pbarBath4.ForegroundTrailingSize2 = 2;
            this.pbarBath4.Location = new System.Drawing.Point(35, 114);
            this.pbarBath4.Marquee = Bornander.UI.ProgressBar.ProgressBar.MarqueeStyle.TileWrap;
            this.pbarBath4.MarqueeWidth = 10;
            this.pbarBath4.Maximum = 100;
            this.pbarBath4.Minimum = 0;
            this.pbarBath4.Name = "pbarBath4";
            this.pbarBath4.OverlayDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath4.OverlayLeadingSize = 4;
            this.pbarBath4.OverlayPicture = null;
            this.pbarBath4.OverlayTrailingSize = 4;
            this.pbarBath4.Size = new System.Drawing.Size(148, 10);
            this.pbarBath4.Type = Bornander.UI.ProgressBar.ProgressBar.BarType.Progress;
            this.pbarBath4.Value = 0;
            this.pbarBath4.Value2 = 0;
            // 
            // pbarBath5
            // 
            this.pbarBath5.BackgroundDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath5.BackgroundLeadingSize = 2;
            this.pbarBath5.BackgroundPicture = ((System.Drawing.Image)(resources.GetObject("pbarBath5.BackgroundPicture")));
            this.pbarBath5.BackgroundTrailingSize = 2;
            this.pbarBath5.ForegroundDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath5.ForegroundDrawMethod2 = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath5.ForegroundLeadingSize = 2;
            this.pbarBath5.ForegroundLeadingSize2 = 2;
            this.pbarBath5.ForegroundPicture = ((System.Drawing.Image)(resources.GetObject("pbarBath5.ForegroundPicture")));
            this.pbarBath5.ForegroundPicture2 = ((System.Drawing.Image)(resources.GetObject("pbarBath5.ForegroundPicture2")));
            this.pbarBath5.ForegroundTrailingSize = 2;
            this.pbarBath5.ForegroundTrailingSize2 = 2;
            this.pbarBath5.Location = new System.Drawing.Point(35, 130);
            this.pbarBath5.Marquee = Bornander.UI.ProgressBar.ProgressBar.MarqueeStyle.TileWrap;
            this.pbarBath5.MarqueeWidth = 10;
            this.pbarBath5.Maximum = 100;
            this.pbarBath5.Minimum = 0;
            this.pbarBath5.Name = "pbarBath5";
            this.pbarBath5.OverlayDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath5.OverlayLeadingSize = 4;
            this.pbarBath5.OverlayPicture = null;
            this.pbarBath5.OverlayTrailingSize = 4;
            this.pbarBath5.Size = new System.Drawing.Size(148, 10);
            this.pbarBath5.Type = Bornander.UI.ProgressBar.ProgressBar.BarType.Progress;
            this.pbarBath5.Value = 0;
            this.pbarBath5.Value2 = 0;
            // 
            // pbarBath6
            // 
            this.pbarBath6.BackgroundDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath6.BackgroundLeadingSize = 2;
            this.pbarBath6.BackgroundPicture = ((System.Drawing.Image)(resources.GetObject("pbarBath6.BackgroundPicture")));
            this.pbarBath6.BackgroundTrailingSize = 2;
            this.pbarBath6.ForegroundDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath6.ForegroundDrawMethod2 = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath6.ForegroundLeadingSize = 2;
            this.pbarBath6.ForegroundLeadingSize2 = 2;
            this.pbarBath6.ForegroundPicture = ((System.Drawing.Image)(resources.GetObject("pbarBath6.ForegroundPicture")));
            this.pbarBath6.ForegroundPicture2 = ((System.Drawing.Image)(resources.GetObject("pbarBath6.ForegroundPicture2")));
            this.pbarBath6.ForegroundTrailingSize = 2;
            this.pbarBath6.ForegroundTrailingSize2 = 2;
            this.pbarBath6.Location = new System.Drawing.Point(35, 146);
            this.pbarBath6.Marquee = Bornander.UI.ProgressBar.ProgressBar.MarqueeStyle.TileWrap;
            this.pbarBath6.MarqueeWidth = 10;
            this.pbarBath6.Maximum = 100;
            this.pbarBath6.Minimum = 0;
            this.pbarBath6.Name = "pbarBath6";
            this.pbarBath6.OverlayDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath6.OverlayLeadingSize = 4;
            this.pbarBath6.OverlayPicture = null;
            this.pbarBath6.OverlayTrailingSize = 4;
            this.pbarBath6.Size = new System.Drawing.Size(148, 10);
            this.pbarBath6.Type = Bornander.UI.ProgressBar.ProgressBar.BarType.Progress;
            this.pbarBath6.Value = 0;
            this.pbarBath6.Value2 = 0;
            // 
            // pbarBath7
            // 
            this.pbarBath7.BackgroundDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath7.BackgroundLeadingSize = 2;
            this.pbarBath7.BackgroundPicture = ((System.Drawing.Image)(resources.GetObject("pbarBath7.BackgroundPicture")));
            this.pbarBath7.BackgroundTrailingSize = 2;
            this.pbarBath7.ForegroundDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath7.ForegroundDrawMethod2 = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath7.ForegroundLeadingSize = 2;
            this.pbarBath7.ForegroundLeadingSize2 = 2;
            this.pbarBath7.ForegroundPicture = ((System.Drawing.Image)(resources.GetObject("pbarBath7.ForegroundPicture")));
            this.pbarBath7.ForegroundPicture2 = ((System.Drawing.Image)(resources.GetObject("pbarBath7.ForegroundPicture2")));
            this.pbarBath7.ForegroundTrailingSize = 2;
            this.pbarBath7.ForegroundTrailingSize2 = 2;
            this.pbarBath7.Location = new System.Drawing.Point(35, 162);
            this.pbarBath7.Marquee = Bornander.UI.ProgressBar.ProgressBar.MarqueeStyle.TileWrap;
            this.pbarBath7.MarqueeWidth = 10;
            this.pbarBath7.Maximum = 100;
            this.pbarBath7.Minimum = 0;
            this.pbarBath7.Name = "pbarBath7";
            this.pbarBath7.OverlayDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath7.OverlayLeadingSize = 4;
            this.pbarBath7.OverlayPicture = null;
            this.pbarBath7.OverlayTrailingSize = 4;
            this.pbarBath7.Size = new System.Drawing.Size(148, 10);
            this.pbarBath7.Type = Bornander.UI.ProgressBar.ProgressBar.BarType.Progress;
            this.pbarBath7.Value = 0;
            this.pbarBath7.Value2 = 0;
            // 
            // pbarBath8
            // 
            this.pbarBath8.BackgroundDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath8.BackgroundLeadingSize = 2;
            this.pbarBath8.BackgroundPicture = ((System.Drawing.Image)(resources.GetObject("pbarBath8.BackgroundPicture")));
            this.pbarBath8.BackgroundTrailingSize = 2;
            this.pbarBath8.ForegroundDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath8.ForegroundDrawMethod2 = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath8.ForegroundLeadingSize = 2;
            this.pbarBath8.ForegroundLeadingSize2 = 2;
            this.pbarBath8.ForegroundPicture = ((System.Drawing.Image)(resources.GetObject("pbarBath8.ForegroundPicture")));
            this.pbarBath8.ForegroundPicture2 = ((System.Drawing.Image)(resources.GetObject("pbarBath8.ForegroundPicture2")));
            this.pbarBath8.ForegroundTrailingSize = 2;
            this.pbarBath8.ForegroundTrailingSize2 = 2;
            this.pbarBath8.Location = new System.Drawing.Point(35, 177);
            this.pbarBath8.Marquee = Bornander.UI.ProgressBar.ProgressBar.MarqueeStyle.TileWrap;
            this.pbarBath8.MarqueeWidth = 10;
            this.pbarBath8.Maximum = 100;
            this.pbarBath8.Minimum = 0;
            this.pbarBath8.Name = "pbarBath8";
            this.pbarBath8.OverlayDrawMethod = Bornander.UI.ProgressBar.ProgressBar.DrawMethod.Stretch;
            this.pbarBath8.OverlayLeadingSize = 4;
            this.pbarBath8.OverlayPicture = null;
            this.pbarBath8.OverlayTrailingSize = 4;
            this.pbarBath8.Size = new System.Drawing.Size(148, 10);
            this.pbarBath8.Type = Bornander.UI.ProgressBar.ProgressBar.BarType.Progress;
            this.pbarBath8.Value = 0;
            this.pbarBath8.Value2 = 0;
            // 
            // tmrRefresh
            // 
            this.tmrRefresh.Interval = 3000;
            this.tmrRefresh.Tick += new System.EventHandler(this.tmrRefresh_Tick);
            // 
            // lblOverall
            // 
            this.lblOverall.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblOverall.ForeColor = System.Drawing.Color.Gray;
            this.lblOverall.Location = new System.Drawing.Point(186, 191);
            this.lblOverall.Name = "lblOverall";
            this.lblOverall.Size = new System.Drawing.Size(46, 16);
            this.lblOverall.Text = " ";
            // 
            // frmLessenOver
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.lblOverall);
            this.Controls.Add(this.lblWholeBathSM);
            this.Controls.Add(this.lblWholeBathUSM);
            this.Controls.Add(this.pbarUltraShortTermMem);
            this.Controls.Add(this.pbarShortTermMem);
            this.Controls.Add(this.lblExpiredBathUSM);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.lblExpiredBathSM);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.lblWholeBathNL);
            this.Controls.Add(this.lblWholeBath8);
            this.Controls.Add(this.lblWholeBath7);
            this.Controls.Add(this.lblWholeBath6);
            this.Controls.Add(this.lblWholeBath5);
            this.Controls.Add(this.lblWholeBath4);
            this.Controls.Add(this.lblWholeBath3);
            this.Controls.Add(this.lblWholeBath2);
            this.Controls.Add(this.lblWholeBath1);
            this.Controls.Add(this.lblExpiredBath8);
            this.Controls.Add(this.lblExpiredBath7);
            this.Controls.Add(this.lblExpiredBath6);
            this.Controls.Add(this.lblExpiredBath5);
            this.Controls.Add(this.lblExpiredBath4);
            this.Controls.Add(this.lblExpiredBath3);
            this.Controls.Add(this.lblExpiredBath2);
            this.Controls.Add(this.lblExpiredBath1);
            this.Controls.Add(this.pbarNotLearned);
            this.Controls.Add(this.pbarBath1);
            this.Controls.Add(this.pbarBath2);
            this.Controls.Add(this.pbarBath3);
            this.Controls.Add(this.pbarBath4);
            this.Controls.Add(this.pbarBath5);
            this.Controls.Add(this.pbarBath6);
            this.Controls.Add(this.pbarBath7);
            this.Controls.Add(this.pbarBath8);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label28);
            this.Menu = this.mainMenu1;
            this.Name = "frmLessenOver";
            this.Text = "Lessen Overview";
            this.Deactivate += new System.EventHandler(this.frmLessenOver_Deactivate);
            this.Load += new System.EventHandler(this.frmLessenOver_Load);
            this.Closed += new System.EventHandler(this.frmLessenOver_Closed);
            this.Activated += new System.EventHandler(this.frmLessenOver_Activated);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem menuItem2;
        private System.Windows.Forms.MenuItem menuItem3;
        private System.Windows.Forms.MenuItem menuItem4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.MenuItem mnuRptLearnNew;
        private System.Windows.Forms.MenuItem menuItem6;
        private System.Windows.Forms.MenuItem menuItem8;
        private System.Windows.Forms.MenuItem menuItem7;
        private Bornander.UI.ProgressBar.ProgressBar pbarBath8;
        private Bornander.UI.ProgressBar.ProgressBar pbarBath7;
        private Bornander.UI.ProgressBar.ProgressBar pbarBath6;
        private Bornander.UI.ProgressBar.ProgressBar pbarBath5;
        private Bornander.UI.ProgressBar.ProgressBar pbarBath4;
        private Bornander.UI.ProgressBar.ProgressBar pbarBath3;
        private Bornander.UI.ProgressBar.ProgressBar pbarBath2;
        private Bornander.UI.ProgressBar.ProgressBar pbarBath1;
        private Bornander.UI.ProgressBar.ProgressBar pbarNotLearned;
        private System.Windows.Forms.MenuItem mnuRptBath1;
        private System.Windows.Forms.MenuItem mnuRptBath2;
        private System.Windows.Forms.MenuItem mnuRptBath3;
        private System.Windows.Forms.MenuItem mnuRptBath4;
        private System.Windows.Forms.MenuItem mnuRptBath5;
        private System.Windows.Forms.MenuItem mnuRptBath6;
        private System.Windows.Forms.MenuItem mnuRptBath7;
        private System.Windows.Forms.Label lblExpiredBath8;
        private System.Windows.Forms.Label lblExpiredBath7;
        private System.Windows.Forms.Label lblExpiredBath6;
        private System.Windows.Forms.Label lblExpiredBath5;
        private System.Windows.Forms.Label lblExpiredBath4;
        private System.Windows.Forms.Label lblExpiredBath3;
        private System.Windows.Forms.Label lblExpiredBath2;
        private System.Windows.Forms.Label lblExpiredBath1;
        private System.Windows.Forms.Label lblWholeBath8;
        private System.Windows.Forms.Label lblWholeBath7;
        private System.Windows.Forms.Label lblWholeBath6;
        private System.Windows.Forms.Label lblWholeBath5;
        private System.Windows.Forms.Label lblWholeBath4;
        private System.Windows.Forms.Label lblWholeBath3;
        private System.Windows.Forms.Label lblWholeBath2;
        private System.Windows.Forms.Label lblWholeBath1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label lblWholeBathSM;
        private System.Windows.Forms.Label lblWholeBathUSM;
        private System.Windows.Forms.Label lblWholeBathNL;
        private System.Windows.Forms.MenuItem mnuSave;
        private System.Windows.Forms.MenuItem mnuSaveAs;
        private System.Windows.Forms.MenuItem menuItem9;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.MenuItem mnuRptNl;
        private System.Windows.Forms.MenuItem mnuRptUsm;
        private System.Windows.Forms.MenuItem mnuRptStm;
        private System.Windows.Forms.Label lblExpiredBathSM;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblExpiredBathUSM;
        private System.Windows.Forms.Label label16;
        private Bornander.UI.ProgressBar.ProgressBar pbarShortTermMem;
        private Bornander.UI.ProgressBar.ProgressBar pbarUltraShortTermMem;
        private System.Windows.Forms.Timer tmrRefresh;
        private System.Windows.Forms.Label lblOverall;
    }
}