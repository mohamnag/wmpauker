﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;


namespace WMPauker
{
    public partial class frmMain : Form
    {
        public static PaukerClass paClass;
        public static string fileName;

        public frmMain()
        {
           
            InitializeComponent();

            this.Height = pictureBox1.Height;
            this.Width = pictureBox1.Width;
            this.Top = (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2;
            this.Left = (Screen.PrimaryScreen.Bounds.Width - this.Width) / 2;
            tmrSwitch.Enabled = true;
        }

        private void tmrSwitch_Tick(object sender, EventArgs e)
        {
            this.Hide();
            (new frmOpen()).Show();
            tmrSwitch.Enabled = false;
        }

        public static void showAbout()
        {
            (new frmAbout()).ShowDialog();
        }

        public static void exitApplication()
        {
            //we need just one exit point all over the application
            if (paClass != null && paClass.needsSave && MessageBox.Show("Do you want to save data before exit?", "Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                paClass.saveToFile(fileName);
            Application.Exit();
        }

    }
}