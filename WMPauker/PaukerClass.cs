﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Xml;
using ICSharpCode.SharpZipLib.GZip;
using System.IO;

namespace WMPauker
{

    /// <summary>
    /// This is common class for core pauker related works
    /// </summary>
    public class PaukerClass
    {

        public class font
        {
            public int Background;
            public int Foreground;
            public bool Bold;
            public bool Italic;
            public string Family;
            public byte Size;

            public font()
            {
                Background = -1;
                Foreground = -16777216;
                Bold = false;
                Italic = false;
                Family = "Tahoma";
                Size = 12;
            }
        }

        public class cardSide
        {
            public enum Orient : byte { LTR, RTL };

            public string text;
            public Orient Orientation;
            public bool RepeatByTyping;
            public font font;

            public cardSide()
            {
                font = new font();
            }
        }

        public class card
        {
            public cardSide front;
            public cardSide back;
            public Int64 learnedTimeStamp;

            public card()
            {
                front = new cardSide();
                back = new cardSide();
            }
        }

        public class batch
        {
            internal sbyte batchNumber;
            public Int32 Count
            {
                get { return Cards.Count; }
            }
            public List<card> Cards;
            public Int32 expiredCount
            {
                get
                {
                    //calc the count of expired cards and return it
                    return countExpired();
                }
            }

            private Int32 countExpired()
            {
                Int32 Result = 0;
                Int64 minLearnTime = 0;
                if (batchNumber == -2)          //not learned =-> all are expired!
                    return this.Count;
                else if (batchNumber == -1)     //ultra short term mem
                    minLearnTime = 999 + 1000 * (getUnixTimeStamp(DateTime.Now) - PaukerClass.ultraShortTermMemExpiritySecconds);
                else if (batchNumber == 0)      // short term mem
                    minLearnTime = 999 + 1000 * (getUnixTimeStamp(DateTime.Now) - PaukerClass.shortTermMemExpirityMinutes * 60);
                else if (batchNumber > 0)       //other batchs
                    minLearnTime = 999 + 1000 * (getUnixTimeStamp(DateTime.Now) - ((Int64)Math.Pow(Math.E, batchNumber - 1) * 24 * 60 * 60));

                foreach (card c in Cards)
                {
                    if ((c.learnedTimeStamp) < minLearnTime)
                        Result++;
                }

                return Result;
            }
        }

        public Dictionary<string, batch> batchs;
        public Int32 Count
        {
            get
            {
                Int32 result = 0;
                foreach (batch b in batchs.Values)
                {
                    result += b.Count;
                }
                return result;
            }
        }
        public string Description;
        public static byte ultraShortTermMemExpiritySecconds = 18;
        public static byte shortTermMemExpirityMinutes = 12;
        private bool changed = false;
        public bool needsSave
        {
            get
            {
                return changed;
            }
        }

        public void approveCard(string batchKey, Int32 cardNo)
        {
            string nextBatchKey;

            //add the card numbered cardNo to next batch and update it's time remove it from current batch
            switch (batchKey)
            {
                case "nl":
                    nextBatchKey = "usm";
                    break;
                case "usm":
                    nextBatchKey = "stm";
                    break;
                case "stm":
                    nextBatchKey = "1";
                    break;
                case "1":
                    nextBatchKey = "2";
                    break;
                case "2":
                    nextBatchKey = "3";
                    break;
                case "3":
                    nextBatchKey = "4";
                    break;
                case "4":
                    nextBatchKey = "5";
                    break;
                case "5":
                    nextBatchKey = "6";
                    break;
                case "6":
                    nextBatchKey = "7";
                    break;
                case "7":
                    nextBatchKey = "8";
                    break;
                case "8":
                    //an unreached level! what to do next?
                    return;
                default:
                    return;
            }

            batchs[batchKey.ToString()].Cards[cardNo].learnedTimeStamp = getUnixTimeStamp(DateTime.Now) * 1000;
            batchs[nextBatchKey].Cards.Add(batchs[batchKey.ToString()].Cards[cardNo]);
            batchs[batchKey].Cards.Remove(batchs[batchKey].Cards[cardNo]);

            changed = true;
        }

        public void resetCard(string batchKey, Int32 cardNo)
        {
            //add the card numbered cardNo to not learned batch and update it's time remove it from current batch
            batchs["nl"].Cards.Add(batchs[batchKey].Cards[cardNo]);
            batchs[batchKey].Cards.Remove(batchs[batchKey].Cards[cardNo]);

            changed = true;
        }

        private List<card> extractCards(XmlNodeList node)
        {
            List<card> cards = new List<card>(node.Count);
            XmlNode sideNode;
            XmlNode fontNode;

            for (int i = 0; i < node.Count; i++)
            {
                cards.Add(new card());

                sideNode = getFrontSide(node[i].ChildNodes);
                fontNode = getFont(sideNode.ChildNodes);

                cards[i].front.text = sideNode.InnerText;
                cards[i].front.Orientation =
                    sideNode.Attributes["Orientation"].Value == "LTR" ? cardSide.Orient.LTR : cardSide.Orient.RTL;
                cards[i].learnedTimeStamp =
                    sideNode.Attributes["LearnedTimestamp"] == null ? 0 :
                    Convert.ToInt64(sideNode.Attributes["LearnedTimestamp"].Value);
                cards[i].front.RepeatByTyping = Convert.ToBoolean(sideNode.Attributes["RepeatByTyping"].Value);
                if (fontNode != null)
                {
                    cards[i].front.font.Bold = Convert.ToBoolean(fontNode.Attributes["Bold"].Value);
                    cards[i].front.font.Family = fontNode.Attributes["Family"].Value;
                    cards[i].front.font.Italic = Convert.ToBoolean(fontNode.Attributes["Italic"].Value);
                    cards[i].front.font.Size = Convert.ToByte(fontNode.Attributes["Size"].Value);
                    //Background
                    //Foreground
                }

                sideNode = getReverseSide(node[i].ChildNodes);
                fontNode = getFont(sideNode.ChildNodes);

                cards[i].back.text = sideNode.InnerText;
                cards[i].back.Orientation =
                    sideNode.Attributes["Orientation"].Value == "LTR" ? cardSide.Orient.LTR : cardSide.Orient.RTL;
                cards[i].back.RepeatByTyping = Convert.ToBoolean(sideNode.Attributes["RepeatByTyping"].Value);
                if (fontNode != null)
                {
                    cards[i].back.font.Bold = Convert.ToBoolean(fontNode.Attributes["Bold"].Value);
                    cards[i].back.font.Family = fontNode.Attributes["Family"].Value;
                    cards[i].back.font.Italic = Convert.ToBoolean(fontNode.Attributes["Italic"].Value);
                    cards[i].back.font.Size = Convert.ToByte(fontNode.Attributes["Size"].Value);
                    //Background
                    //Foreground
                }
            }

            return cards;
        }

        private XmlNode getFrontSide(XmlNodeList nodes)
        {
            for (byte i = 0; i < nodes.Count; i++)
            {
                if (nodes[i].Name.ToLower() == "frontside")
                    return nodes[i];
            }

            return null;
        }

        private XmlNode getReverseSide(XmlNodeList nodes)
        {
            for (byte i = 0; i < nodes.Count; i++)
            {
                if (nodes[i].Name.ToLower() == "reverseside")
                    return nodes[i];
            }

            return null;
        }

        private XmlNode getFont(XmlNodeList nodes)
        {
            for (byte i = 0; i < nodes.Count; i++)
            {
                if (nodes[i].Name.ToLower() == "font")
                    return nodes[i];
            }

            return null;
        }

        public PaukerClass(string fileName)
        {
            byte[] data;
            batchs = new Dictionary<string, batch>();

            //initialize the Pauker object
            if (!System.IO.File.Exists(fileName))
            {
                throw new Exception("File does not exists");
            }

            System.IO.FileStream fst = System.IO.File.Open(fileName, System.IO.FileMode.Open);

            data = new byte[fst.Length];
            if (fst.Read(data, 0, data.Length) != fst.Length)
            {
                throw new System.IO.IOException("Problem reading the file");
            }

            //decompress data if it's a compressed one
            if (fileName.IndexOf(".gz") > 0)
            {
                data = InflateData(data);
            }

            //now i have xml data in data array
            //i have to parse the data into my own array
            //of pauker data

            XmlDocument xmlDoc = new XmlDocument();
            System.IO.MemoryStream ist = new System.IO.MemoryStream(data);
            xmlDoc.Load(ist);

            Description = xmlDoc.DocumentElement.GetElementsByTagName("Description")[0].InnerText;

            XmlNode tmpNode;

            //load cards into batchs
            for (int j = 1; j <= 11; j++)
            {
                tmpNode = xmlDoc.DocumentElement.ChildNodes[j];

                switch (j)
                {
                    case 1:
                        batchs.Add("nl", new batch());
                        batchs["nl"].batchNumber = -2;
                        //if (j < xmlDoc.DocumentElement.ChildNodes.Count)
                        batchs["nl"].Cards = (tmpNode != null ? extractCards(tmpNode.ChildNodes) : new List<card>());
                        break;

                    case 2:
                        batchs.Add("usm", new batch());
                        batchs["usm"].batchNumber = -1;
                        //if (j < xmlDoc.DocumentElement.ChildNodes.Count)
                        batchs["usm"].Cards = (tmpNode != null ? extractCards(tmpNode.ChildNodes) : new List<card>());
                        break;

                    case 3:
                        batchs.Add("stm", new batch());
                        batchs["stm"].batchNumber = 0;
                        //if (j < xmlDoc.DocumentElement.ChildNodes.Count)
                        batchs["stm"].Cards = (tmpNode != null ? extractCards(tmpNode.ChildNodes) : new List<card>());
                        break;

                    case 4:
                        batchs.Add("1", new batch());
                        batchs["1"].batchNumber = 1;
                        batchs["1"].Cards = (tmpNode != null ? extractCards(tmpNode.ChildNodes) : new List<card>());
                        break;

                    case 5:
                        batchs.Add("2", new batch());
                        batchs["2"].batchNumber = 2;
                        batchs["2"].Cards = (tmpNode != null ? extractCards(tmpNode.ChildNodes) : new List<card>());
                        break;

                    case 6:
                        batchs.Add("3", new batch());
                        batchs["3"].batchNumber = 3;
                        batchs["3"].Cards = (tmpNode != null ? extractCards(tmpNode.ChildNodes) : new List<card>());
                        break;

                    case 7:
                        batchs.Add("4", new batch());
                        batchs["4"].batchNumber = 4;
                        batchs["4"].Cards = (tmpNode != null ? extractCards(tmpNode.ChildNodes) : new List<card>());
                        break;

                    case 8:
                        batchs.Add("5", new batch());
                        batchs["5"].batchNumber = 5;
                        batchs["5"].Cards = (tmpNode != null ? extractCards(tmpNode.ChildNodes) : new List<card>());
                        break;

                    case 9:
                        batchs.Add("6", new batch());
                        batchs["6"].batchNumber = 6;
                        batchs["6"].Cards = (tmpNode != null ? extractCards(tmpNode.ChildNodes) : new List<card>());
                        break;

                    case 10:
                        batchs.Add("7", new batch());
                        batchs["7"].batchNumber = 7;
                        batchs["7"].Cards = (tmpNode != null ? extractCards(tmpNode.ChildNodes) : new List<card>());
                        break;

                    case 11:
                        batchs.Add("8", new batch());
                        batchs["8"].batchNumber = 8;
                        batchs["8"].Cards = (tmpNode != null ? extractCards(tmpNode.ChildNodes) : new List<card>());
                        break;
                }
            }

            //now we have every thing for a constructor!
        }

        private static long getUnixTimeStamp(System.DateTime date_time_convert)
        {
            TimeSpan span = date_time_convert.ToUniversalTime() - new DateTime(1970, 1, 1, 11, 30, 0, 0);

            return (long)span.TotalSeconds;
        }

        /// <summary>
        /// Compress the raw data using gzip compression
        /// </summary>
        /// <param name="rawData"></param>
        /// <returns></returns>
        private static byte[] DeflateData(byte[] data)
        {
            MemoryStream stream = new MemoryStream();
            GZipOutputStream gzipOut = new GZipOutputStream(stream);
            gzipOut.Write(data, 0, data.Length);
            gzipOut.Finish();
            return stream.ToArray();
        }

        /// <summary>
        /// Decompress the data using gzip compression
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private static byte[] InflateData(byte[] data)
        {
            MemoryStream ms = new MemoryStream();
            ms.Write(data, 0, data.Length);
            ms.Seek(0, SeekOrigin.Begin);
            Stream gzin = new GZipInputStream(ms);
            MemoryStream decompMemStream = new MemoryStream();
            int size = 2048;
            byte[] buffer = new byte[size];
            while (size > 0)
            {
                size = gzin.Read(buffer, 0, size);
                if (size > 0)
                    decompMemStream.Write(buffer, 0, size);
            }

            return decompMemStream.ToArray();
        }

        public void saveToFile(string fileName)
        {
            byte[] data;

            if (batchs["usm"].Count > 0 || batchs["stm"].Count > 0)
                throw new Exception("Can't save while you have cards on Ultra short term or Short term batchs");

            System.IO.MemoryStream stream = new MemoryStream();
            XmlTextWriter xmlWr = new XmlTextWriter(stream, Encoding.UTF8);

            xmlWr.WriteStartDocument(false);
            xmlWr.WriteComment("This is a lesson file for Pauker (http://pauker.sourceforge.net) Created By WMPauker (http://wmpauker.khone.ir)");

            //start Lesson
            xmlWr.WriteStartElement("Lesson");
            xmlWr.WriteStartAttribute("LessonFormat");
            xmlWr.WriteString("1.7");
            xmlWr.WriteEndAttribute();

            //write description
            xmlWr.WriteStartElement("Description");
            xmlWr.WriteString(this.Description);
            xmlWr.WriteEndElement();

            writeBatch(xmlWr, batchs["nl"]);
            writeBatch(xmlWr, batchs["usm"]);
            writeBatch(xmlWr, batchs["stm"]);
            for (int i = 3; i < batchs.Count; i++)
            {
                writeBatch(xmlWr, batchs[(i - 2).ToString()]);
            }

            xmlWr.WriteEndDocument();

            //we have everything in stream.GetBuffer()
            xmlWr.Flush();
            data = stream.ToArray();

            //compress data if requested
            if (fileName.IndexOf(".gz") > 0)
            {
                data = DeflateData(data);
            }

            try
            {

                System.IO.FileStream fst = System.IO.File.Open(fileName, System.IO.FileMode.Create, FileAccess.Write);
                fst.Write(data, 0, data.Length);
                if (fst.Position != data.Length)
                    throw new IOException("Unsecessful write!");
            }
            catch (Exception e)
            {
                throw e;
            }

            changed = false;
        }

        private void writeBatch(XmlTextWriter xmlWr, batch tmpBatch)
        {
            xmlWr.WriteStartElement("Batch");

            for (int i = 0; i < tmpBatch.Count; i++)
            {
                xmlWr.WriteStartElement("Card");

                writeSide(xmlWr, tmpBatch.Cards[i].front, true, tmpBatch.Cards[i].learnedTimeStamp);
                writeSide(xmlWr, tmpBatch.Cards[i].back);

                xmlWr.WriteEndElement();
            }

            xmlWr.WriteEndElement();
        }

        private void writeSide(XmlTextWriter xmlWr, cardSide tmpSide)
        {
            writeSide(xmlWr, tmpSide, false, 0);
        }

        private void writeSide(XmlTextWriter xmlWr, cardSide tmpSide, bool front, long learnTime)
        {
            //start side
            xmlWr.WriteStartElement(front ? "FrontSide" : "ReverseSide");

            if (front && learnTime > 0)
            {
                xmlWr.WriteStartAttribute("LearnedTimestamp");
                xmlWr.WriteString(learnTime.ToString());
                xmlWr.WriteEndAttribute();
            }

            xmlWr.WriteStartAttribute("Orientation");
            xmlWr.WriteString(tmpSide.Orientation.ToString());
            xmlWr.WriteEndAttribute();

            xmlWr.WriteStartAttribute("RepeatByTyping");
            xmlWr.WriteString(tmpSide.RepeatByTyping.ToString());
            xmlWr.WriteEndAttribute();

            xmlWr.WriteStartElement("Text");
            xmlWr.WriteString(tmpSide.text);
            xmlWr.WriteEndElement();

            //front font
            xmlWr.WriteStartElement("Font");

            xmlWr.WriteStartAttribute("Background");
            xmlWr.WriteString(tmpSide.font.Background != 0 ? tmpSide.font.Background.ToString() : "-1");
            xmlWr.WriteEndAttribute();

            xmlWr.WriteStartAttribute("Bold");
            xmlWr.WriteString(tmpSide.font.Bold.ToString());
            xmlWr.WriteEndAttribute();

            xmlWr.WriteStartAttribute("Family");
            xmlWr.WriteString(tmpSide.font.Family);
            xmlWr.WriteEndAttribute();

            xmlWr.WriteStartAttribute("Foreground");
            xmlWr.WriteString(tmpSide.font.Foreground != 0 ? tmpSide.font.Foreground.ToString() : "-16777216");
            xmlWr.WriteEndAttribute();

            xmlWr.WriteStartAttribute("Italic");
            xmlWr.WriteString(tmpSide.font.Italic.ToString());
            xmlWr.WriteEndAttribute();

            xmlWr.WriteStartAttribute("Size");
            xmlWr.WriteString(tmpSide.font.Size.ToString());
            xmlWr.WriteEndAttribute();

            //end font
            xmlWr.WriteEndElement();

            //end side
            xmlWr.WriteEndElement();
        }


    }
}
