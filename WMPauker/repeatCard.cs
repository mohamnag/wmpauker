﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WMPauker
{

    public partial class repeatCard : Form
    {
        string batch;
        //Int32 CardNo;

        public repeatCard(string batch)
        {
            InitializeComponent();

            lblTitle.Text += "'" + getBatchName(batch) + "'";

            this.batch = batch;
            lblRemaining.Text = frmMain.paClass.batchs[batch].expiredCount.ToString();
            if (lblRemaining.Text == "0")
                throw new Exception("No expired cards!");
            lblTrue.Text = "0";
            lblFalse.Text = "0";
            showNextCard();
        }

        private string getBatchName(string batchKey)
        {
            try
            {
                if (batchKey == "nl")
                    return "Not learned";
                else if (batchKey == "usm")
                    return "Ultra short term";
                else if (batchKey == "stm")
                    return "Short term";
                else if (Convert.ToInt32(batchKey) > 0)
                    return batchKey;
            }
            catch { }
            return "";
        }

        private void showNextCard()
        {
            if (frmMain.paClass.batchs[batch].Cards.Count == 0 || frmMain.paClass.batchs[batch].expiredCount == 0)
            {
                this.Close();
                return;
            }

            //first check for short term or ultra short term mem
            if (batch != "usm" && batch != "stm")
                chkShortTermMems();

            lblResult.Visible = false;

            if (batch == "nl")
            {
                txbReverse.Visible = true;
                lblNl.Visible = true;
                this.Menu = mnuNl;
            }
            else
            {
                txbReverse.Visible = false;
                lblTest.Visible = true;
                this.Menu = mnuTest;
            }
                        
            //we have to set all font properties here:
            txbFront.Text = frmMain.paClass.batchs[batch].Cards[0].front.text;
            txbReverse.Text = frmMain.paClass.batchs[batch].Cards[0].back.text;

        }

        private void chkShortTermMems()
        {
            if (frmMain.paClass.batchs["usm"].expiredCount > 0)
            {
                MessageBox.Show("You have expired cards on 'Ultra Short Term' batch.\nFirst you have to repeat them!",
                    "Ultra short term", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
                repeatCard cardFrm = new repeatCard("usm");
                cardFrm.ShowDialog();
            }
            if (frmMain.paClass.batchs["stm"].expiredCount > 0)
            {
                MessageBox.Show("You have expired cards on 'Short Term' batch.\nFirst you have to repeat them!",
                    "Short term", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
                repeatCard cardFrm = new repeatCard("stm");
                cardFrm.ShowDialog();
            }
        }

        private void mnuStop_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void mnuShowResult_Click(object sender, EventArgs e)
        {
            showResult();
        }

        private void showResult()
        {
            this.Menu = mnuResult;
            lblTest.Visible = false;
            lblResult.Visible = true;
            txbReverse.Visible = true;
        }

        private void mnuAprvCard_Click(object sender, EventArgs e)
        {
            frmMain.paClass.approveCard(batch, 0);
            //CardNo++;
            lblRemaining.Text = frmMain.paClass.batchs[batch].expiredCount.ToString();

            lblTrue.Text = Convert.ToString((Convert.ToInt32(lblTrue.Text)) + 1);

            showNextCard();
        }

        private void mnuResetCard_Click(object sender, EventArgs e)
        {
            frmMain.paClass.resetCard(batch, 0);
            lblRemaining.Text = frmMain.paClass.batchs[batch].expiredCount.ToString();
            lblFalse.Text = Convert.ToString((Convert.ToInt32(lblFalse.Text)) + 1);

            showNextCard();
        }

    }
}